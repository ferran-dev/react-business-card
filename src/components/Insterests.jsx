export default function Interests() {
  return (
    <div className="section">
      <h3 className="section-title">Interests</h3>
      <p className="section-content">
        Among the classic ones (reading, watching movies, etc.), I am an astronomy lover, a music geek, and an inner growth seeker.
      </p>
    </div>
  )
}