import profile_pic from '../assets/profile.jpg'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons'

export default function Info() {
  return (
    <div className='info section'>
      <img src={profile_pic} alt="Card Picture" />
      <h1 className='info--name'>Ferran Pla</h1>
      <h2 className='info--role'>Frontend Developer</h2>
      <div className='info--btn-row'>
        <a className='btn email-button' href='mailto:plasitjes.ferran@gmail.com'><FontAwesomeIcon icon={faEnvelope} />Email</a>
        <a className='btn linkedin-button' href='https://www.linkedin.com/in/ferran-pla-sitjes/'><FontAwesomeIcon icon={faLinkedin} />LinkedIn</a>
      </div>
    </div>
  )
}