export default function About() {
  return (
    <div className="section">
      <h3 className="section-title">About</h3>
      <p className="section-content">I am a front-end developer with a focus on good design and ease of use. I am always eager to learn and looking to make the most of my time.</p>
    </div>
  )
}