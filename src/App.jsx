import './App.css'
import Info from './components/Info.jsx'
import About from './components/About'
import Interests from './components/Insterests'
import Footer from './components/Footer'

export default function App() {
  return (
    <div className='card'>
      <Info/>
      <About/>
      <Interests />
      <Footer />
    </div>
  )
}
